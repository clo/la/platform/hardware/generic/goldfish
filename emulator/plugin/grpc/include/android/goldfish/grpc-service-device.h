// Copyright 2024 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include <stdbool.h>

typedef struct DisplayChangeListener;
typedef struct DisplaySurface;
typedef struct GrpcDeviceConfiguration {
    char* addr;
    char* tls_cer;
    char* tls_key;
    char* tls_ca;
    char* allowlist;
    char* avd;
    bool use_token;
    int idle_timeout;
    int port;
} GrpcDeviceConfiguration;

bool initialize(GrpcDeviceConfiguration* device);
void finalize(GrpcDeviceConfiguration* device);

// UI related callbacks
void grpc_dpy_gfx_update(struct DisplayChangeListener* dcl, int x, int y, int w, int h);
void grpc_dpy_gfx_switch(struct DisplayChangeListener* dcl, struct DisplaySurface* new_surface);