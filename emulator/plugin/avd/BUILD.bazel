load("@rules_cc//cc:defs.bzl", "cc_binary", "cc_library")

cc_library(
    name = "avd_hdrs",
    hdrs = [
        "include/android/goldfish/avd-finalize.h",
        "include/android/goldfish/avd-info.h",
    ],
    includes = ["include"],
    visibility = [
        "//visibility:public",
    ],
    deps = [
        "//hardware/generic/goldfish/emulator/devices:connector_registry_hdrs",
    ],
)

cc_library(
    name = "qemu-looper",
    srcs = [
        "src/android/goldfish/qemu-looper.cpp",
    ],
    hdrs = [
        "include/android/goldfish/qemu-looper.h",
    ],
    copts = ["-Wno-extern-c-compat"],
    includes = ["include"],
    visibility = [
        "//visibility:public",
    ],
    deps = [
        "//external/qemu:libmodule-common",
        "//external/qemu:qemu-headers",
        "//hardware/generic/goldfish/android/looper",
        "//hardware/generic/goldfish/android/memory",
        "//hardware/generic/goldfish/android/sockets",
        "//hardware/generic/goldfish/android/threads",
        "//hardware/generic/goldfish/emulator/adb/logger",
        "//hardware/generic/goldfish/emulator/config",
        "@com_google_absl//absl/log",
        "@com_google_absl//absl/log:check",
        "@com_google_absl//absl/log:globals",
        "@com_google_absl//absl/log:initialize",
        "@com_google_absl//absl/log:log_sink_registry",
    ],
    alwayslink = True,
)

cc_library(
    name = "avd_impl",
    srcs = [
        "src/android/goldfish/adb-device.cpp",
        "src/android/goldfish/avd-finalize.cpp",
        "src/android/goldfish/avd-info.cpp",
    ],
    copts = ["-Wno-extern-c-compat"],
    visibility = [
        "//visibility:public",
    ],
    deps = [
        ":avd_hdrs",
        ":qemu-looper",
        "//external/qemu:libmodule-common",
        "//external/qemu:qemu-headers",
        "//hardware/generic/goldfish/android/looper",
        "//hardware/generic/goldfish/android/memory",
        "//hardware/generic/goldfish/android/sockets",
        "//hardware/generic/goldfish/android/threads",
        "//hardware/generic/goldfish/emulator/adb/host",
        "//hardware/generic/goldfish/emulator/config",
        "//hardware/generic/goldfish/emulator/devices:connector_registry_impl",
        "//hardware/generic/goldfish/emulator/hal/clipboard",
        "//hardware/generic/goldfish/emulator/hal/fingerprint",
        "//hardware/generic/goldfish/emulator/hal/gps",
        "//hardware/generic/goldfish/emulator/hal/guest-status",
        "//hardware/generic/goldfish/emulator/hal/sensors",
        "//hardware/generic/goldfish/emulator/plugin/battery",
        "//hardware/generic/goldfish/emulator/plugin/virtio-input-android",
        "//hardware/generic/goldfish/emulator/plugin/vsock_goldfish:goldfish_vsock_hdrs",
        "@com_google_absl//absl/log",
        "@com_google_absl//absl/log:check",
        "@com_google_absl//absl/log:globals",
        "@com_google_absl//absl/log:initialize",
        "@com_google_absl//absl/log:log_sink_registry",
    ],
    alwayslink = True,
)

cc_binary(
    name = "avd",
    linkshared = True,
    visibility = [
        "//visibility:public",
    ],
    deps = [":avd_impl"] +
           select({
               "@platforms//os:windows": [
                   "//external/qemu:qemu-system-x86_64.if",
                   "//hardware/generic/goldfish/emulator/plugin/vsock_goldfish:virtio-vsock-goldfish-impl-x86_64",
               ],
               "//conditions:default": [],
           }),
)
