// Copyright (C) 2018 The Android Open Source Project
//
// This software is licensed under the terms of the GNU General Public
// License version 2, as published by the Free Software Foundation, and
// may be copied, distributed, and modified under those terms.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

#include "android/utils/tempfile.h"

#include <gtest/gtest.h>

#include <string_view>

#include "aemu/base/files/PathUtils.h"
#include "android/base/file/file_io.h"
#include "android/base/system/System.h"

static bool fileExists(const std::string& filename) {
    return android::base::System::get()->pathExists(filename);
}

TEST(tempfile, createTemp) {
    TempFile* tempFile = tempfile_create();
    const char* filePath = tempfile_path(tempFile);
    EXPECT_NE(filePath, nullptr);
    EXPECT_TRUE(fileExists(filePath));
    tempfile_close(tempFile);
    EXPECT_FALSE(fileExists(filePath));
}

TEST(tempfile, createTempWithExt) {
    const char* ext = ".ext";
    TempFile* tempFile = tempfile_create_with_ext(ext);
    const char* filePath = tempfile_path(tempFile);
    EXPECT_NE(filePath, nullptr);
    EXPECT_TRUE(fileExists(filePath)) << filePath << " should exist";
    int pos = strlen(filePath) - strlen(ext);
    EXPECT_GT(pos, 0);
    EXPECT_EQ(0, strcmp(filePath + pos, ext));
    tempfile_close(tempFile);
    EXPECT_FALSE(fileExists(filePath));
}

TEST(tempfile, createAndDoubleClose) {
    TempFile* tempFile = tempfile_create();
    std::string filePath = tempfile_path(tempFile);
    EXPECT_TRUE(fileExists(filePath));
    tempfile_unref_and_close(filePath.c_str());
    EXPECT_FALSE(fileExists(filePath));
    // Expect no error or anything
    tempfile_unref_and_close(filePath.c_str());
    EXPECT_FALSE(fileExists(filePath));
}

TEST(tempfile, badClose) {
    auto gtestTmpDir = android::base::System::get()->getTempDir();
    const char* filename = "tempfile";

#ifdef _WIN32
    std::string filePath =
            android::base::Win32UnicodeString(std::wstring(gtestTmpDir / filename).c_str())
                    .toString();
#else
    auto filePath = gtestTmpDir / filename;
#endif
    FILE* fd = android_fopen(filePath.c_str(), "w");
    fclose(fd);
    EXPECT_TRUE(fileExists(filePath));
    tempfile_unref_and_close(filePath.c_str());
    EXPECT_TRUE(fileExists(filePath));
    EXPECT_TRUE(android::base::System::get()->deleteFile(filePath));
    EXPECT_FALSE(fileExists(filePath));
}
