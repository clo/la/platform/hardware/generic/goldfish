

// Copyright (C) 2024 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "absl/status/status.h"

#include "disk_drive.h"

namespace android::goldfish {

class EncryptionDrive : public MutableDiskDrive {
  public:
    explicit EncryptionDrive(const HardwareConfig& hw) : MutableDiskDrive("encrypt", "06.0") {
        mDiskId = "encrypt";
        mDiskImage = fs::path(hw.disk_encryptionKeyPartition_path);
        mDiskImage.replace_extension(".img.qcow2");
    }
    absl::Status initialize(const Emulator& emulator) override;
};
}  // namespace android::goldfish