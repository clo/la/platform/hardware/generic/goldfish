// Copyright 2015 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "android/crashreport/CrashReporter.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include <cmath>
#include <filesystem>
#include <fstream>
#include <map>
#include <ostream>
#include <string>
#include <utility>
#include <vector>

#include "aemu/base/files/PathUtils.h"
#include "android/base/bazel/bazel_info.h"
#include "android/base/system/System.h"
#include "android/crashreport/SimpleStringAnnotation.h"
#include "base/files/file_path.h"
#include "client/annotation.h"
#include "client/crash_report_database.h"
#include "client/crashpad_client.h"
#include "client/settings.h"
#include "hardware/generic/goldfish/emulator/launcher/aemu_version.h"
#include "host-common/crash-handler.h"
#include "util/misc/uuid.h"

#ifdef _WIN32
#include <io.h>

#include "base/strings/utf_string_conversions.h"
#else
#include <signal.h>
#endif

using android::base::Bazel;
using android::base::c_str;
using android::base::PathUtils;
using android::base::System;
using base::FilePath;
namespace fs = std::filesystem;

namespace android {
namespace crashreport {

const constexpr char kCrashpadDatabase[] = "emu-dev-crash-" VERSION ".db";
// The crashpad handler binary, as shipped with the emulator.
const constexpr char kCrashpadHandler[] = "crashpad_handler";

using DefaultStringAnnotation = crashpad::StringAnnotation<1024>;

CrashReporter::CrashReporter() {}

FilePath CrashReporter::databaseDirectory() {
    auto database_directory = System::get()->envGet("ANDROID_EMU_CRASH_REPORTING_DATABASE");
    if (!database_directory.empty()) {
#ifdef _WIN32
        return ::base::FilePath(::base::UTF8ToWide(database_directory));
#else
        return ::base::FilePath(database_directory);
#endif
    }
    auto crashDatabasePath = System::get()->getTempDir() / std::string(kCrashpadDatabase);

    return FilePath(crashDatabasePath);
}

FilePath CrashReporter::handlerExe() {
    fs::path handler;
    auto from_env = System::get()->getEnvironmentVariable("AEMU_CRASHPAD_HANDLER");

    if (!from_env.empty()) {
        handler = fs::path(from_env);
    } else if (Bazel::inBazel()) {
        handler = fs::path(Bazel::runfilesPath("com_google_crashpad/handler/crashpad_handler"));
    } else {
        handler = System::get()->findBundledExecutable(kCrashpadHandler);
    }
    return FilePath(handler);
}

HangDetector& CrashReporter::hangDetector() {
    if (!mHangDetector) {
        mHangDetector = std::make_unique<HangDetector>(
                [](auto message) { CrashReporter::get()->die(c_str(message)); });
    }

    return *mHangDetector;
}

CrashReporter* CrashReporter::get() {
    static CrashReporter reporter;
    return &reporter;
}

static void enableSignalTermination() {
#if defined(__APPLE__) || defined(__linux__)
    // We will not get crash reports without the signals below enabled.
    sigset_t set;
    sigemptyset(&set);
    sigaddset(&set, SIGTERM);
    sigaddset(&set, SIGHUP);
    sigaddset(&set, SIGFPE);
    sigaddset(&set, SIGINT);
    sigaddset(&set, SIGSEGV);
    sigaddset(&set, SIGABRT);
    sigaddset(&set, SIGILL);
    int result = pthread_sigmask(SIG_UNBLOCK, &set, nullptr);
    if (result != 0) {
        LOG(WARNING) << "Could not set thread sigmask: " << result;
    }
#endif
}

void CrashReporter::die(const char* message) {
    // Make sure we can actually register a crash on this thread.
    enableSignalTermination();

    addMessage(message);
    // this is the most cross-platform way of crashing
    // any other I know about has its flaws:
    //  - abort() isn't caught by Breakpad on Windows
    //  - null() may screw the callstack
    //  - explicit *null = 1 can be optimized out
    //  - requesting dump and exiting later has a very noticeable delay in
    //    between, so some real crash could stick in the middle
    volatile int* volatile ptr = nullptr;
    *ptr = 1313;  // die

#ifndef _WIN32
    raise(SIGABRT);
#endif
    abort();  // make compiler believe it doesn't return
}

void CrashReporter::addMessage(const char* message) {
    mAnnotationLog << message;
}

void CrashReporter::attachData(std::string name, std::string data, bool replace) {
    // Let's figure out how many bytes we need in our annotations.
    // We will bucketize by power of 2. We take the floor because
    // 2<<1 == 2^2, 2<<2 == 2^3, ..., etc..
    int shift = floor(log2(data.size()));

    std::unique_ptr<crashpad::Annotation> annotation;
    // Sadly we have to do some pseudo switching to minimize the use of
    // space in our minidump.
    if (shift <= 5)
        // 2<<5 == 2^6 == 64
        annotation = std::make_unique<SimpleStringAnnotation<2 << 5>>(name, data);
    if (shift == 6) annotation = std::make_unique<SimpleStringAnnotation<2 << 6>>(name, data);
    if (shift == 7) annotation = std::make_unique<SimpleStringAnnotation<2 << 7>>(name, data);
    if (shift == 8) annotation = std::make_unique<SimpleStringAnnotation<2 << 8>>(name, data);
    if (shift == 9) annotation = std::make_unique<SimpleStringAnnotation<2 << 9>>(name, data);
    if (shift == 10) annotation = std::make_unique<SimpleStringAnnotation<2 << 10>>(name, data);
    if (shift == 11) annotation = std::make_unique<SimpleStringAnnotation<2 << 11>>(name, data);
    if (shift == 12) annotation = std::make_unique<SimpleStringAnnotation<2 << 12>>(name, data);
    if (shift >= 13) {
        annotation = std::make_unique<SimpleStringAnnotation<2 << 13>>(name, data);
        if (data.size() > 2 << 13)
            LOG(WARNING) << "Crash annotation is very large (" << data.size()
                         << "), only 16384 bytes will be recorded, " << data.size() - (2 << 13)
                         << " bytes are lost.";
    }

    mAnnotations.push_back(std::move(annotation));
}

}  // namespace crashreport
}  // namespace android

using android::crashreport::CrashReporter;

extern "C" {

void crashhandler_append_message(const char* message) {
    CrashReporter::get()->addMessage(message);
}

void crashhandler_append_message_format_v(const char* format, va_list args) {
    char message[2048] = {};
    vsnprintf(message, sizeof(message) - 1, format, args);
    crashhandler_append_message(message);
}

void crashhandler_append_message_format(const char* format, ...) {
    va_list args;
    va_start(args, format);
    crashhandler_append_message_format_v(format, args);
    va_end(args);
}

void crashhandler_die(const char* message) {
    LOG(ERROR) << "crashing system with message: " << message;
    CrashReporter::get()->die(message);
}

void __attribute__((noreturn)) crashhandler_die_format_v(const char* format, va_list args) {
    char message[2048] = {};
    vsnprintf(message, sizeof(message) - 1, format, args);
    crashhandler_die(message);
}

void crashhandler_die_format(const char* format, ...) {
    va_list args;
    va_start(args, format);
    crashhandler_die_format_v(format, args);
}

void crashhandler_add_string(const char* name, const char* string) {
    CrashReporter::get()->attachData(name, string);
}

void crashhandler_exitmode(const char* message) {
    CrashReporter::get()->addMessage(message);
}

bool crashhandler_copy_attachment(const char* destination, const char* source) {
    std::ifstream sourceFile(PathUtils::asUnicodePath(source).c_str(), std::ios::binary);
    std::stringstream buffer;
    buffer << sourceFile.rdbuf();

    if (sourceFile.bad()) {
        return false;
    }

    CrashReporter::get()->attachData(destination, buffer.str());
    return true;
}

}  // extern "C"
